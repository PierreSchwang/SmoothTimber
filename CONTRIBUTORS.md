
# Contributors

A huge thanks to...

* Pierre who did a great job improving/fixing the CoreProtect Support and adding in LogBlock support!<br/>[[Github](https://github.com/PierreSchwang)/[Gitlab](https://gitlab.com/PierreSchwang)/[Spigot](https://www.spigotmc.org/members/pierreschwang.936938/)/[Polymart](https://polymart.org/user/pierreschwang.786)]